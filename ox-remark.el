;;; ox-remark.el --- Org export backend for remark presentation slides  -*- lexical-binding: t; -*-

;; Copyright (C) 2010-2021 Jonas D. Großekathöfer


;; Author: Jonas D. Großekathöfer <mail@grszkth.fr>
;; Created: 13 Oct 2022

;; Keywords: languages
;; URL: https://gitlab.com/grszkthfr/ox-remark

(org-export-define-derived-backend 'remark 'html
  :menu-entry
  '(?h "Export to HTML"
       ((?r "As HTML (Remark)" (lambda (a s v b) (org-remark-export-to-html a s v)))
        (?R "As HTML and open (Remark)"
            (lambda (a s v b)
              (if a (org-md-export-to-remark t s v)
                (org-open-file (org-remark-export-to-html nil s v)))))))
  :translate-alist '((bold . org-md-bold)
                     (center-block . org-md--convert-to-html)
                     (code . org-md-verbatim)
                     (drawer . org-md--identity)
                     (dynamic-block . org-md--identity)
                     (example-block . org-md-example-block)
                     (export-block . org-md-export-block)
                     (fixed-width . org-md-example-block)
                     (headline . org-md-headline)
                     (horizontal-rule . org-md-horizontal-rule)
                     (inline-src-block . org-md-verbatim)
                     (inlinetask . org-md--convert-to-html)
                     (inner-template . org-md-inner-template)
                     (italic . org-md-italic)
                     (item . org-md-item)
                     (keyword . org-md-keyword)
                     (latex-environment . org-md-latex-environment)
                     (latex-fragment . org-md-latex-fragment)
                     (line-break . org-md-line-break)
                     (link . org-md-link)
                     (node-property . org-md-node-property)
                     (paragraph . org-md-paragraph)
                     (plain-list . org-md-plain-list)
                     (plain-text . org-remark-plain-text) ;; do not replace ---
                     (property-drawer . org-md-property-drawer)
                     (quote-block . org-md-quote-block)
                     (section . org-md-section)
                     (special-block . org-remark-special-block) ; notes block as speaker notes
                     (src-block . org-md-example-block)
                     (table . org-md--convert-to-html)
                     (template . org-remark-template)
                     (verbatim . org-md-verbatim))
  :options-alist
  ;; KEYWORD, OPTIONS, DEFAULT, BEHAVIOR (treatment of multiple keywords/options (nil -> Keep old, t -> Take new, space/newline/split -> concate with space/.../))
  '((:with-toc nil "toc" org-remark-export-with-toc)
    (:md-footnote-format nil nil org-md-footnote-format)
    (:md-footnotes-section nil nil org-md-footnotes-section)
    (:md-headline-style nil nil org-md-headline-style)
    ;; (:remark-lib-dir) ; found in xaringan. relevant?

    ;; Configuration, found in #+OPTIONS: lines
    ;; for Details see https://github.com/gnab/remark/wiki/Configuration
    ;; General
    (:remark-ratio nil "remark_ratio" org-remark-ratio t)                             
    (:remark-navigation-scroll nil "remark_navigation_scroll" remark-navigation-scroll t)
    (:remark-navigation-touch nil "remark_navigation_touch" remark-navigation-touch t)
    (:remark-navigation-click nil "remark_navigation_click" remark-navigation-click t)
    ;; (:remark-timer-start-on-change nil "remark_timer_start_on_change" remark-timer-start-on-change t)
    ;; (:remark-timer-resetable nil "remark_timer_resetable" remark-timer-resetable t)
    ;; (:remark-timer-enabled nil "remark_timer_enabled" remark-timer-enabled t)
    ;; (:remark-timer-formatter nil "remark_timer_formatter" remark-timer-formatter t)
    ;; (:remark-slide-numer-format nil "remark_slide_numer_format" remark-slide-numer-format t)
    (:remark-count-incremental-slides nil "remark_count_incremental_slides" remark-count-incremental-slides t)
    ;; (:remark-source nil "remark_source" remark-source t)
    ;; (:remark-source-url nil "remark_source_url" remark-source-url t)
    ;; (:remark-include-presener-notes nil "remark_include_presener_notes" remark-include-presener-notes t)

    ;; ;; Highlighting
    ;; (:remark-highlight-language nil "remark_highlight_language" DEFAULT t)	    ; Default: -
    ;; (:remark-highlight-style nil "remark_highlight_style" DEFAULT t)		    ; Default: default
    ;; (:remark-highlight-lines nil "remark_highlight_lines" DEFAULT t)		    ; Default: false; Alt: true
    ;; (:remark-highlight-spans nil "remark_highlight_spans" DEFAULT t)		    ; Default: false; Alt: true

    ;; ;; Formatting, in ATTR_REMARKS, Details see https://github.com/gnab/remark/wiki/Markdown Slide properties
    ;; (:remark-class "remark_class" nil org-remark-class space)	                        ; Defaults: horizontal: top; Alt: middle, bottom; vertical: left; Alt: center, right
    ;; ;; (:remark-name "remark_name" nil DEFAULT t)
    ;; ;; (:remark-content "remark_content" nil DEFAULT t) 		        ; Default: left, Alt: center, right; can be handeld via #+attr_html
    ;; (:remark-background-image "remark-background-image" nil "" t)		; url(image.jpg)
    ;; (:remark-background-position "remark_background_position" nil DEEFAULT t)	; Default: center; Alt: ?
    ;; (:remark-background-repeat "remark_background_repeat" nil DEEFAULT t)	; Default: no-repeat; Alt: ?
    ;; (:remark-background-size "remark_background_size" nil DEEFAULT t)		; Default: contain; Alt: ?
    ;; (:remark-count "remark_count" nil DEEFAULT t)
    ;; (:remark-template "remark_template" nil DEEFAULT t)
    ;; (:remark-layout "remark_layout" nil DEEFAULT t)
    ;; (:remark-exclude "remark_exclude" nil DEEFAULT t) ; Default: false; Alt: true
    ))

(defun org-remark-plain-text (text info)
  "Transcode a TEXT string into Markdown format.
TEXT is the string to transcode.  INFO is a plist holding
contextual information."
  (when (plist-get info :with-smart-quotes)
    (setq text (org-export-activate-smart-quotes text :html info)))
  ;; The below series of replacements in `text' is order sensitive.
  ;; Protect `, *, _, and \
  (setq text (replace-regexp-in-string "[`*_\\]" "\\\\\\&" text))
  ;; Protect ambiguous #.  This will protect # at the beginning of
  ;; a line, but not at the beginning of a paragraph.  See
  ;; `org-md-paragraph'.
  (setq text (replace-regexp-in-string "\n#" "\n\\\\#" text))
  ;; Protect ambiguous !
  (setq text (replace-regexp-in-string "\\(!\\)\\[" "\\\\!" text nil nil 1))
  ;; Handle special strings, if required.
  (when (plist-get info :with-special-strings)
    (setq text (org-remark-convert-special-strings text)))
  ;; Handle break preservation, if required.
  (when (plist-get info :preserve-breaks)
    (setq text (replace-regexp-in-string "[ \t]*\n" "  \n" text)))
  ;; Return value.
  text)

(defun org-remark-convert-special-strings (string)
  "Convert special characters in STRING to HTML."
  (dolist (a org-remark-special-string-regexps string)
    (let ((re (car a))
          (rpl (cdr a)))
      (setq string (replace-regexp-in-string re rpl string t)))))

(defconst org-remark-special-string-regexps
  '(("\\\\-" . "&#x00ad;")		; shy
  ;; ("---\\([^-]\\)" . "&#x2014;\\1")	; mdash
  ;; ("--\\([^-]\\)" . "&#x2013;\\1")	; ndash
  ("\\.\\.\\." . "&#x2026;"))		; hellip
  "Regular expressions for special string conversion.")

(defun org-remark-special-block (special-block contents info)
  "Transcode a SPECIAL-BLOCK element from Org to Reveal.
CONTENTS holds the contents of the block. INFO is a plist
holding contextual information.
If the block type is 'NOTES', transcode the block into a
remark slide note. Otherwise, export the block as by the HTML
exporter."
  (let ((block-type (org-element-property :type special-block)))
    (if (string= (upcase block-type) "NOTES")
          (format "???\n%s\n" contents)
      (org-md-special-block special-block contents info))))

(defgroup org-export-remark nil
  "Options for exporting Orgmode files to remark pressentations."
  :tag "Org Export Remark"
  :group 'org-export)

(defcustom org-remark-export-with-toc nil
  "Non-nil means create a table of contents in exported files.

The table of contents contains headlines with levels up to
`org-export-headline-levels'.

When this variable is set to an integer N, include levels up to
N in the table of contents.  Although it may then be different
from `org-export-headline-levels', it is cannot be larger than
the number of headline levels.

When nil, no table of contents is created.

This option can also be set with the OPTIONS keyword,
e.g. \"toc:nil\" or \"toc:3\"."
  :group 'org-export-general
  :type '(choice
          (const :tag "No Table of Contents" nil)
          (const :tag "Full Table of Contents" t)
          (integer :tag "TOC to level"))
  :safe (lambda (x)
          (or (booleanp x)
              (integerp x))))

(defcustom org-remark-ratio "4:3"
  "Set the slideshow display ratio"
  :group 'org-export-remark
  :type 'string)

(defcustom remark-navigation-scroll t
  "Enable or disable navigating using scroll"
  :group 'org-export-remark
  :type 'boolean)

(defcustom remark-navigation-touch t
  "Enable or disable navigation using touch"
  :group 'org-export-remark
  :type 'boolean)

(defcustom remark-navigation-click nil
  "Enable or disable navigation using click"
  :group 'org-export-remark
  :type 'boolean)

(defcustom remark-timer-start-on-change t
  "Start timer when first change occurs"
  :group 'org-export-remark
  :type 'boolean)

(defcustom remark-timer-resetable t
  "Is it possible to reset the timer"
  :group 'org-export-remark
  :type 'boolean)

(defcustom remark-timer-enabled t
"Is the timer enabled"
:group 'org-export-remark
    :type 'boolean)

(defcustom remark-timer-formatter "H:mm:ss"
  "A formatter for the elapsed time in milliseconds"
  :group 'org-export-remark
  :type 'string)

(defcustom remark-slide-numer-format "Slide %current% of %total%"
  "Customize slide number label, (either*) using a format string"
  :group 'org-export-remark
  :type 'string)

(defcustom remark-count-incremental-slides t
  "Enable or disable counting of incremental slides in the slide counting"
  :group 'org-export-remark
  :type 'boolean)

(defcustom remark-source "undefined"
  "Provide a source markdown for slides explicitly as an option 
instead of the textarea with id=\"source\"
Alternatives: 'Slide 1\n---\nSlide 2', ..."
  :group 'org-export-remark
  :type 'string)

(defcustom remark-source-url "undefined"
  "Read source markdown for slides from URL (local or external) instead of the textarea with id=\"source\"
Alternatives: 'some_file.md', 'https://example.host.com/file.md', ..."
  :group 'org-export-remark
  :type 'string)

(defcustom remark-include-presener-notes t
  "Value indicates if presenter notes should be visible or not"
  :group 'org-export-remark
  :type 'boolean)

(defun org-remark-export-to-html
    (&optional async subtreep visible-only body-only ext-plist)
  "Export current buffer to a HTML file.

If narrowing is active in the current buffer, only export its
narrowed part.

If a region is active, export that region.

A non-nil optional argument ASYNC means the process should happen
asynchronously.  The resulting file should be accessible through
the `org-export-stack' interface.

When optional argument SUBTREEP is non-nil, export the sub-tree
at point, extracting information from the headline properties
first.

When optional argument VISIBLE-ONLY is non-nil, don't export
contents of hidden elements.

When optional argument BODY-ONLY is non-nil, only write code
between \"<body>\" and \"</body>\" tags.

EXT-PLIST, when provided, is a property list with external
parameters overriding Org default settings, but still inferior to
file-local settings.

Return output file's name."
  (interactive)
  (let* ((extension (concat
                     (when (> (length org-html-extension) 0) ".")
                     (or (plist-get ext-plist :html-extension)
                         org-html-extension
                         "html")))
         (file (org-export-output-file-name extension subtreep))
         (org-export-coding-system org-html-coding-system))
    (org-export-to-file 'remark file
      async subtreep visible-only body-only ext-plist)))

(defun org-remark--if-format (fmt val)
  "Apply `format' to FMT and VAL if VAL is a number or non-empty string.
Otherwise, return empty string."
  (if (or (numberp val)
          (and (stringp val) (> (length val) 0)))
      (format fmt val)
    ""))

;; this is where all fall together
(defun org-remark-template (contents info)
  "Return complete document string after HTML conversion.
CONTENTS is the transcoded contents string.
PROPERTY is a plist holding export options."
  (concat
   "<!DOCTYPE html>\n"
   "<html>"
   "<head>\n"
   (org-remark--if-format "<title>%s</title>\n"
                          (org-export-data (plist-get info :title) info))
   "<meta charset=\"utf-8\">\n"
   (org-remark--if-format "<meta name=\"author\" content=\"%s\">\n"
                          (org-export-data (plist-get info :author) info))

   "<script src=\"libs/header-attrs/header-attrs.js\"></script>\n"
   "<link href=\"libs/remark-css/default.css\" rel=\"stylesheet\" />\n"
   "<link href=\"libs/remark-css/default-fonts.css\" rel=\"stylesheet\" />\n"
   "<link rel=\"stylesheet\" href=\"libs/custom.css\" type=\"text/css\" />\n"

   "</head>\n"
   "<body>\n"
   "<textarea id=\"source\">\n"
   contents
   "</textarea>\n"
   "<script src=\"https://remarkjs.com/downloads/remark-latest.min.js\">\n"
   "</script>\n"
   "<script>\n"
   "var slideshow = remark.create({\n"
   (org-remark--if-format "ratio: \"%s\",\n"
                          (org-export-data (plist-get info :remark-ratio) info))
   "navigation: {\n"
   (format "\tscroll: %s,\n"
           (org-export-data (if (plist-get info :remark-navigation-scroll) "true" "false") info))
   (format "\ttouch: %s,\n"
           (org-export-data (if (plist-get info :remark-navigation-touch) "true" "false") info))
   (format "\tclick: %s,\n},\n"
           (org-export-data (if (plist-get info :remark-navigation-click) "true" "false") info))
   ;; (org-remark--if-format "\nXX\": \"%s\",\n"
   ;;                        (org-export-data (plist-get info :remark-timer-start-on-change) info))
   ;; (org-remark--if-format "\n\"XX\": \"%s\",\n"
   ;;                        (org-export-data (plist-get info :remark-timer-resetable) info))
   ;; (org-remark--if-format "\n\"XX\": \"%s\",\n"
   ;;                        (org-export-data (plist-get info :remark-timer-enabled) info))
   ;; (org-remark--if-format "\n\"XX\": \"%s\",\n"
   ;;                        (org-export-data (plist-get info :remark-timer-formatter) info))
   ;; (org-remark--if-format "\n\"XX\": \"%s\",\n"
   ;;                        (org-export-data (plist-get info :remark-slide-numer-format) info))
   (format "\ncountIncrementalSlides: %s,\n"
           (org-export-data (if (plist-get info :remark-count-incremental-slides) "true" "false") info))
   ;; (org-remark--if-format "\n\"XX\": \"%s\",\n"
   ;;                        (org-export-data (plist-get info :remark-source) info))
   ;; (org-remark--if-format "\n\"XX\": \"%s\",\n"
   ;;                        (org-export-data (plist-get info :remark-source-url) info))
   ;; (org-remark--if-format "\n\"XX\": \"%s\",\n"
   ;;                        (org-export-data (plist-get info :remark-include-presener-notes) info))
   "});\n"
   "</script>\n"
   "</body>\n"
   "</html>\n"
   ))
